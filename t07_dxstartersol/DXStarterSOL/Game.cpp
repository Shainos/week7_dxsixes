#include "Game.h"

Game::Game()
{

}

Game::~Game()
{

}
//initialise game values
void Game::Initialise()
{
	Current = GameState::INITIALISING;
	srand(static_cast<unsigned>(time(0)));
	maxrolls = 0;
	score = 0;
	dice = 0;
	rolls = 0;

	Current = GameState::INPUT_ROLLS;
}
//changing the max number of rols
void Game::SetMaxRolls(int amount)
{	
	if (Current == GameState::INPUT_ROLLS)
	{
		//removing the last value
		if (amount == -1)
		{
			maxrolls = maxrolls / 10;
		}
		else
		{
			//adds the number by changing the place value
			if (maxrolls < 100000000)//prevents and integar overflow
				maxrolls = (maxrolls * 10) + amount;
		}
	}
	
	
}

//logic for rolling the dice
void Game::RollDice()
{

	rolls++;
	//checks if the 
	if (rolls <= maxrolls)
	{
		dice = Roll();
		if (dice == 6)
		{
			score++;
		}
	}
	else
	{
		Current = GameState::FINISHED_ROLLING;
	}
}
//generats a random dice value(1-6)
int Game::Roll()
{
	return ((rand() % 6) + 1);
}

//deteermines the message to be displayed depending on what state the game is in
void Game::Update()
{
	switch (Current)
	{
	case Game::INITIALISING:
		message1 = "";
		message2 = "";
		message3 = "";
		break;
	case Game::INPUT_ROLLS:
		message1 = "How Many Rolls Do You Want?";
		message2 = std::to_string(maxrolls);
		message3 = "Press Enter to Continue";
		break;
	case Game::ROLLING:
		message1 = "Roll " + std::to_string(rolls) + " out of " + std::to_string(maxrolls);
		message2 = "You Rolled " + std::to_string(dice) + ". Your Score is " +  std::to_string(score);
		message3 = "Press Enter to Continue roling. Press Space to AutoRoll";
		break;
	case Game::FINISHED_ROLLING:
		message1 = "Your Final Score is " + std::to_string(score) + " after " + std::to_string(maxrolls) + " Rolls";
		message2 = "Press Enter to Restart";
		message3 = "Press Escape to Quit";
		break;
	default:
		break;
	}

	
}
//retrieves the message so it can be displayed
std::string Game::GetMsg(int msgval)
{
	if (msgval == 1)
	{
		return message1;
	}
	else if (msgval == 2)
	{
		return message2;
	}
	else
	{
		return message3;
	}
}

//checks moves the program onto the next part
void Game::Progress()
{
	if (Current == GameState::INPUT_ROLLS)
	{
		if (maxrolls > 0)
		{
			Current = GameState::ROLLING;
			RollDice();
		}
		
	}
	else if (Current == GameState::ROLLING)
	{
		RollDice();
	}
	else
	{
		Initialise();
	}

}
//automatically rolls the dice until it reaches the max number of rolls
void Game::AutoRoll()
{
	while (Current == GameState::ROLLING)
	{
		RollDice();
	}
}