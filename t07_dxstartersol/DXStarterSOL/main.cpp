#include <windows.h>
#include <string>
#include <cassert>
#include <d3d11.h>
#include <iomanip>
#include <ctime>

#include "WindowUtils.h"
#include "D3DUtil.h"
#include "D3D.h"
#include "SimpleMath.h"
#include "SpriteFont.h"
#include "Game.h"

using namespace std;
using namespace DirectX;
using namespace DirectX::SimpleMath;


DirectX::SpriteFont *gpFont = nullptr;
DirectX::SpriteBatch *gpSpriteBatch = nullptr;

float gResTimer = 0;
int gFrameCounter = 0;
float gFrameTimer = 0;

char lastChar;
float inputDelay;
Game game;


void InitGame(MyD3D& d3d)
{
	gpSpriteBatch = new SpriteBatch(&d3d.GetDeviceCtx());
	assert(gpSpriteBatch);

	gpFont = new SpriteFont(&d3d.GetDevice(), L"../bin/data/algerian.spritefont");
	assert(gpFont);

	game.Initialise();

}


//any memory or resources we made need releasing at the end
void ReleaseGame()
{
	delete gpFont;
	gpFont = nullptr;

	delete gpSpriteBatch;
	gpSpriteBatch = nullptr;
}

//called over and over, use it to update game logic
void Update(float dTime, MyD3D& d3d)
{

	if (inputDelay > 0)
		inputDelay =  inputDelay - dTime;
	
	game.Update();

}

//called over and over, use it to render things
void Render(float dTime, MyD3D& d3d)
{
	WinUtil& wu = WinUtil::Get();
	
	Vector4 colour = Colours::Green;
	
	d3d.BeginRender(colour);
	gpSpriteBatch->Begin();

	Vector2 scrn{ (float)wu.GetData().clientWidth, (float)wu.GetData().clientHeight };

	Vector2 pos = Vector2(0, 0);

	string msg1 = game.GetMsg(1);

	RECT dim1 = gpFont->MeasureDrawBounds(msg1.c_str(), Vector2(0, 0));
	pos = Vector2{ (scrn.x / 2), pos.y + (dim1.bottom - dim1.top)*1.2f };
	gpFont->DrawString(gpSpriteBatch, msg1.c_str(), pos, Colours::Black, 0, Vector2((float)dim1.right / 2.f, 0));


	string msg2 = game.GetMsg(2);
	
	RECT dim2 = gpFont->MeasureDrawBounds(msg2.c_str(), Vector2(0,0));
	pos = Vector2{ (scrn.x / 2), pos.y + (dim2.bottom-dim2.top)*1.2f };
	gpFont->DrawString(gpSpriteBatch, msg2.c_str(), pos, Colours::Black, 0, Vector2((float)dim2.right/2.f,0));

	string msg3 = game.GetMsg(3);

	RECT dim3 = gpFont->MeasureDrawBounds(msg3.c_str(), Vector2(0, 0));
	pos = Vector2{ (scrn.x / 2), pos.y + (dim3.bottom - dim3.top)*1.2f };
	gpFont->DrawString(gpSpriteBatch, msg3.c_str(), pos, Colours::Black, 0, Vector2((float)dim3.right / 2.f, 0));

	gpSpriteBatch->End();
	d3d.EndRender();
}

//if ALT+ENTER or resize or drag window we might want do
//something like pause the game perhaps, but we definitely
//need to let D3D know what's happened (OnResize_Default).
void OnResize(int screenWidth, int screenHeight, MyD3D& d3d)
{
	gResTimer = GetClock() + 2;
	d3d.OnResize_Default(screenWidth, screenHeight);
}

//messages come from windows all the time, should we respond to any specific ones?
LRESULT CALLBACK MainWndProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	//do something game specific here
	switch (msg)
	{
		// Respond to a keyboard event.
	case WM_CHAR:
		switch (wParam)
		{
		case '1':
			if (lastChar != '1')
			{
				game.SetMaxRolls(1);
				lastChar = '1';
				inputDelay = .1f;
				
			}			
			break;
		case '2':
			if (lastChar != '2')
			{
				game.SetMaxRolls(2);
				lastChar = '2';
				inputDelay = .1f;

			}
			break;
		case '3':
			if (lastChar != '3')
			{
				game.SetMaxRolls(3);
				lastChar = '3';
				inputDelay = .1f;

			}
			break;
		case '4':
			if (lastChar != '4')
			{
				game.SetMaxRolls(4);
				lastChar = '4';
				inputDelay = .1f;

			}
			break;
		case '5':
			if (lastChar != '5')
			{
				game.SetMaxRolls(5);
				lastChar = '5';
				inputDelay = .1f;

			}
			break;
		case '6':
			if (lastChar != '6')
			{
				game.SetMaxRolls(6);
				lastChar = '6';
				inputDelay = .1f;

			}
			break;
		case '7':
			if (lastChar != '7')
			{
				game.SetMaxRolls(7);
				lastChar = '7';
				inputDelay = .1f;

			}
			break;
		case '8':
			if (lastChar != '8')
			{
				game.SetMaxRolls(8);
				lastChar = '8';
				inputDelay = .1f;

			}
			break;
		case '9':
			if (lastChar != '9')
			{
				game.SetMaxRolls(9);
				lastChar = '9';
				inputDelay = .1f;

			}
			break;
		case '0':
			if (lastChar != '0')
			{
				game.SetMaxRolls(0);
				lastChar = '0';
				inputDelay = .1f;

			}
			break;
		case 8: //backspace
			if (lastChar != '-')
			{
				game.SetMaxRolls(-1);
				lastChar = '-';
				inputDelay = .1f;
			}
			break;
		case 32://pressing space
			game.AutoRoll();
			break;
		case 13://pressing enter
			game.Progress();
			break;
		case 27: //escape key
		case 'q':
		case 'Q':
			PostQuitMessage(0);
			return 0;
		
			
		}
		if (inputDelay < 0)
			lastChar = NULL;
	}

	//default message handling (resize window, full screen, etc)
	return WinUtil::Get().DefaultMssgHandler(hwnd, msg, wParam, lParam);
}

//main entry point for the game
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE prevInstance,
				   PSTR cmdLine, int showCmd)
{

	int w(1024), h(768);
	if (!WinUtil::Get().InitMainWindow(w, h, hInstance, "Fezzy", MainWndProc, true))
		assert(false);

	MyD3D d3d;
	if (!d3d.InitDirect3D(OnResize))
		assert(false);
	WinUtil::Get().SetD3D(d3d);
	InitGame(d3d);

	bool canUpdateRender;
	float dTime = 0;
	while (WinUtil::Get().BeginLoop(canUpdateRender))
	{
		if (canUpdateRender)
		{
			Update(dTime, d3d);
			Render(dTime, d3d);
		}
		dTime = WinUtil::Get().EndLoop(canUpdateRender);
		gFrameCounter++;
	}

	ReleaseGame();
	d3d.ReleaseD3D(true);	
	return 0;
}