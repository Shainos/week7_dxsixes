#ifndef GAME_H
#define GAME_H


#include <string>
#include <ctime>
class Game
{
public:
	Game();
	~Game();
	void Initialise();
	void Game::SetMaxRolls(int amount);
	void RollDice();
	int Roll();
	void Update();
	void Progress();
	void AutoRoll();
	std::string GetMsg(int msgval);

	enum GameState
	{
		INITIALISING,
		INPUT_ROLLS,
		ROLLING,
		FINISHED_ROLLING
	};
	GameState Current;
private:
	int score;
	int maxrolls;
	int dice;
	int rolls;

	//messages to be dispalyed to the user
	std::string message1;
	std::string message2;
	std::string message3;
};
#endif // !GAME_H